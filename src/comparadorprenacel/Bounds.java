/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparadorprenacel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author GLIDE-03
 */
public class Bounds {
    
    public static List<String[]> geraReport(String[] cabecalho, List<String[]> dados, List<String[]> dic, String caminhoSalvar) {
        
       
        String[] csvCabecalhoBounds = new String[]{"Tipo de Problema", "Id RedCap", "Número de Ordem", "Instrumento", "Nome da Variável","Questão", "Valor", "Min", "Max"};
        String[] csvCabecalhoMissingValues = new String[]{"Tipo de Problema", "Id RedCap", "Número de Ordem", "Instrumento", "Nome da Variável","Questão"};
        
        List<String[]> idNaoElegivelMasMarcadoComoElegivel = new ArrayList<>();
        List<String[]> idNaoElegivel = new ArrayList<>();

        List<String[]> listOutOfBounds = new ArrayList<>();
        listOutOfBounds.add(csvCabecalhoBounds);
        List<String[]> listMissingValues = new ArrayList<>();
        listMissingValues.add(csvCabecalhoMissingValues);
        
        List<String[]> listMissingHygia = new ArrayList<>();
        
        int idxDic = 0, idxColunas = 0, numgest = 0;
        
        int totalPrenacel = 0,
                totalParceiro = 0,
                totalUbsPrenacel = 0,
                totalMorbidade = 0,
                totalRecusa = 0,
                totalNaoDigitado = 0;
        
        //Limpa colunas
        List<String[]> dataFramePrenacel = new ArrayList<>();
        List<Integer> indexColComplete = new ArrayList<>();
        String[] colname;

        
//        while (idxDic < cabecalho.length) {
//            if (cabecalho[idxDic].contains("outr")) {
//                System.out.println(cabecalho[idxDic] + ", ");
//                //indexColComplete.add(idxDic);
//            }
//            idxDic = idxDic + 1;
//        }
//               
        //Seleciona o indice das colunas que tem o complete
        while (idxDic < cabecalho.length) {
            if (cabecalho[idxDic].contains("_complete")) {
                //System.out.println(cabecalho[j] + ", ");
                indexColComplete.add(idxDic);
            }
            idxDic = idxDic + 1;
        }

        System.out.println("Lista de colunas a serem removidas criada");

        int idxString = 0;
        int totalColunasComDados = cabecalho.length - indexColComplete.size();
        colname = new String[totalColunasComDados];
        String[] cabecalhosemcomplete = new String[totalColunasComDados];
        for (String[] array : dados) {
            String[] novoDado = new String[totalColunasComDados];
            idxString = 0;
            //verifica todos os indices dentro do array de completes, se o indice verificado nao eh coluna complete, ele nao insere no novo array
            for (int a=0; a<array.length; a++) {
                if (!indexColComplete.contains(a)) {
                    novoDado[idxString] = array[a];
                    cabecalhosemcomplete[idxString] = cabecalho[a];
                    idxString++;
                }
            }
            dataFramePrenacel.add(novoDado);
        }
        System.out.println("Colunas complete removidas");
                
        idxString = 0;
        for (int a=0; a<cabecalho.length; a++) {
            if (!indexColComplete.contains(a)) {
                colname[idxString] = cabecalho[a];
                idxString++;
            }
        }
        System.out.println("Lista de nomes recarregada");

        for (int i = 0; i < dataFramePrenacel.size(); i++) {
            //System.out.println(i);
            idxDic = 0;
            idxColunas = 0;
            numgest = 0;
            boolean morbidade = false;
            boolean prenacel = false;
            boolean parceiro = false;
            boolean ubsPrenacel = false;
            boolean recusa = false;
            boolean aborto = false;
            
//            if(dataFramePrenacel.get(i)[1].equals("")){
//                idxColunas = dataFramePrenacel.get(0).length;
//                totalNaoDigitado++;
//            }
            
            while (idxColunas < dataFramePrenacel.get(0).length) {
                //String missingValues;
		//System.out.println(colname[idxColunas] +" "+idxColunas + " " + dic.get(idxDic)[0]+" "+idxDic);
                //System.out.println(dic.get(idxDic)[0]+" "+idxDic);
                //System.out.println(idxDic +" "+ dataFramePrenacel.get(0).length);
                
                if (colname[idxColunas].equals(dic.get(idxDic)[0]) || Pattern.matches(dic.get(idxDic)[0]+"___.*", colname[idxColunas])) {
                    //Verifica se o campo está vazio
                    //System.out.println(dic.get(idxDic)[0] +" "+ colname[idxColunas]);
                    
                    int variavelParaApagar = 0;
                    if(dataFramePrenacel.get(i)[0].equals("308"))
                        variavelParaApagar=2;
                    
                    if (dataFramePrenacel.get(i)[idxColunas] == null || dataFramePrenacel.get(i)[idxColunas].equals("") || 
                            (Pattern.matches(dic.get(idxDic)[0], "ender") && 
                            (dataFramePrenacel.get(i)[182].equals("") || dataFramePrenacel.get(i)[313].toLowerCase().contains("recus")))) 
                    {
                        if(!prenacel && idxColunas >= 383 && idxColunas <= 406)
                        {
                            dataFramePrenacel.get(i)[idxColunas] = "$$$";
                        }
                        else if(!morbidade && idxColunas >= 315 && idxColunas <= 382)
                        {
                            dataFramePrenacel.get(i)[idxColunas] = "$$$";
                        }
                        else if(!parceiro && idxColunas >= 407)
                        {
                            dataFramePrenacel.get(i)[idxColunas] = "$$$";
                        }
                        else if(aborto){
                        } 
                        //qdo chega em endereco verifica se a variavel numero de banheiros existe, senao considera recusa
                        else if(Pattern.matches(dic.get(idxDic)[0], "ender") && 
                                //dataFramePrenacel.get(i)[idxColunas].equals("") &&
                                (dataFramePrenacel.get(i)[83].equals("") || 
                                dataFramePrenacel.get(i)[313].toLowerCase().contains("recus"))){
                            //System.out.println(dataFramePrenacel.get(i)[0] + " " + dataFramePrenacel.get(i)[313]);
                            //obs
                            totalRecusa++;
                            dataFramePrenacel.get(i)[idxColunas] = "$$$";
                            recusa = true;
                        }
                        else if(recusa && 
                                ((idxColunas >= 72 && idxColunas <= 314)
                                || (idxColunas >= 371 && idxColunas <= 405))){
                            dataFramePrenacel.get(i)[idxColunas] = "$$$";
                        }
                        else if(Pattern.matches(dic.get(idxDic)[0], "qpnacel") && 
                                ubsPrenacel){
                            dataFramePrenacel.get(i)[idxColunas] = "$$$";
                        }
                         //k. Outro
                        else if((Pattern.matches(dic.get(idxDic)[0], "kmacout") &&
                                Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                                Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 1) ||
                                (Pattern.matches(dic.get(idxDic)[0], "kmacout") &&
                                      dataFramePrenacel.get(i)[idxColunas].equals("")))
                        {
                            dataFramePrenacel.get(i)[idxColunas] = "$$$";
                        }
                        else if(Pattern.matches(dic.get(idxDic)[0], "pontref")){
                        }
                        else if(Pattern.matches(dic.get(idxDic)[0], "obs")){
                        }
                        else if(Pattern.matches(dic.get(idxDic)[0], "mmsobs")){
                        }
                        else if(Pattern.matches(dic.get(idxDic)[0], "obtraz") &&
                                Utils.isInteger(dataFramePrenacel.get(i)[idxColunas+1]) && 
                                Integer.parseInt(dataFramePrenacel.get(i)[idxColunas+1]) == 99){
                        } 
                        else if(Pattern.matches(dic.get(idxDic)[0], "numord")){
                        }
                        else if(Pattern.matches(dic.get(idxDic)[0], "nmmnom")){
                        } 
                        else if(Pattern.matches(dic.get(idxDic)[0], "marido")){
                        } 
                        else if(Pattern.matches(dic.get(idxDic)[0], "telpar")){
                        } 
                        else if(Pattern.matches(dic.get(idxDic)[0], "hygia")){
                            listMissingHygia.add(new String[]{"Hygia vazia",
                            dataFramePrenacel.get(i)[0],
                            dataFramePrenacel.get(i)[6] + dataFramePrenacel.get(i)[7] ,
                            dic.get(idxDic)[1],
                            dic.get(idxDic)[0],
                            dic.get(idxDic)[4]});
                        }
                        else
                        {
                            listMissingValues.add(new String[]{"Variavel vazia",
                            dataFramePrenacel.get(i)[0],
                            dataFramePrenacel.get(i)[6] + dataFramePrenacel.get(i)[7] ,
                            dic.get(idxDic)[1],
                            dic.get(idxDic)[0],
                            dic.get(idxDic)[4]});
                        }
                    } 
                    else if(Pattern.matches(dic.get(idxDic)[0], "ppnacel") && 
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) == 1){
                        prenacel = true;
                        totalPrenacel++;
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "pnacel") && 
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) < 21){
                        ubsPrenacel = true;
                        totalUbsPrenacel++;
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "compli") && 
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) == 1){
                        morbidade = true;
                        totalMorbidade++;
                        
                        if(dataFramePrenacel.get(i)[320].isEmpty() || 
                                dataFramePrenacel.get(i)[320].equals("")|| 
                                dataFramePrenacel.get(i)[320] == null){
                            morbidade = false;
                            
                            listMissingValues.add(new String[]{"Variavel vazia",
                            dataFramePrenacel.get(i)[0],
                            dataFramePrenacel.get(i)[6] + dataFramePrenacel.get(i)[7] ,
                            dic.get(idxDic)[1],
                            "Questionario 2 não digitado"});
                        }
                    }  
                    //verifica se foi gravidez ectopica ou aborto e pula as questoes que nao se aplicam
                    else if(Pattern.matches(dic.get(idxDic)[0], "termges") && 
                            !dataFramePrenacel.get(i)[idxColunas].equals("") &&
                            Utils.isNumeric(dataFramePrenacel.get(i)[idxColunas]) &&
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) > 3){
                        //totalRecusa++;
                        aborto = true;
                    }    
                    else if(Pattern.matches(dic.get(idxDic)[0], "tcle") && 
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) == 1){
                        parceiro = true;
                        totalParceiro++;
                    }   
                    else if (Pattern.matches(dic.get(idxDic)[0], "gestant"))
                    {
                        numgest = Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]);
                        
                        if (numgest <= 8){
                            for(int numgestvazio = numgest; numgestvazio < 9; numgestvazio++){
                                dataFramePrenacel.get(i)[idxColunas + 1 + numgestvazio] = "$$$";
                                dataFramePrenacel.get(i)[idxColunas + 1 + 7 + numgestvazio] = "$$$";
                            }
                        }
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "chefe") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) == 1)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                        dataFramePrenacel.get(i)[idxColunas + 2] = "$$$";
                        dataFramePrenacel.get(i)[idxColunas + 3] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "chefe") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 5)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "ocupa") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 4)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "sitrab") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 4)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "droga") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 1)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "riscout") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 1)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 2] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "motout") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 1)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 2] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "eduout") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 1)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 2] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "coutro") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 1)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "infpre") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 21)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "quedec") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 4 && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 99)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "alivout") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 1)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 2] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "acout") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 1)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 2] = "$$$";
                    }
                    //19. SE FOI CESÁREA,  qual foi a razão? // 20. SE A CESÁREA FOI POR OUTRA RAZÃO, especifique
                    else if(Pattern.matches(dic.get(idxDic)[0], "rcesa") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 25)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    //teve acompanhante
                    else if(Pattern.matches(dic.get(idxDic)[0], "acompa") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) == 1)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 13] = "$$$";
                    }
                    //SE NÃO TEVE ACOMPANHANTE, por quê?
                    else if(Pattern.matches(dic.get(idxDic)[0], "nacomp") &&
                            !dataFramePrenacel.get(i)[idxColunas].equals("") && 
                            dataFramePrenacel.get(i)[idxColunas] != null &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 10)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    //146. Você já ofereceu o peito para o seu bebê?  
                    else if(Pattern.matches(dic.get(idxDic)[0], "mama") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) == 1)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 5] = "$$$";
                    }
                    //Por quê ainda não deu o peito ao seu bebê?
                    else if((Pattern.matches(dic.get(idxDic)[0], "mamanao") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 6) ||
                            (Pattern.matches(dic.get(idxDic)[0], "mamanao") &&
                            dataFramePrenacel.get(i)[idxColunas].equals("$$$")))
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "pretout") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 1)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    //65. Qual o telefone do seu companheiro?
                    else if(Pattern.matches(dic.get(idxDic)[0], "marital") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            (Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) == 3 ||
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) ==  4 ||
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) == 5))
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                        dataFramePrenacel.get(i)[idxColunas + 2] = "$$$";
                    }
                    //INSTRUMENTO 2
                    //11. Por que não foi internada no primeiro hospital/maternidade? (NÃO LER AS OPÇÕES)
                    else if(Pattern.matches(dic.get(idxDic)[0], "pqnprim") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 6)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "ubsenca") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 2)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                        dataFramePrenacel.get(i)[idxColunas + 2] = "$$$";
                        if(Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 6)
                        {
                            dataFramePrenacel.get(i)[idxColunas + 3] = "$$$";
                        }
                    }
                    else if(Pattern.matches(dic.get(idxDic)[0], "motint") &&
                            Utils.isInteger(dataFramePrenacel.get(i)[idxColunas]) && 
                            Integer.parseInt(dataFramePrenacel.get(i)[idxColunas]) != 5)
                    {
                        dataFramePrenacel.get(i)[idxColunas + 1] = "$$$";
                    }
                    //BLOCO H (Q.114- Q.121)
                    //Todo bloco apenas mulheres que responderam 1, 2, ou 3 na questão 16

                    
                    //se nao esta vazio, verifica se esta dentro dos limites
                    else if (dataFramePrenacel.get(i)[idxColunas] != null && !dataFramePrenacel.get(i)[idxColunas].isEmpty()
                            && dic.get(idxDic)[9] != null && !dic.get(idxDic)[9].isEmpty()
                            && dic.get(idxDic)[8] != null && !dic.get(idxDic)[8].isEmpty() && !dic.get(idxDic)[8].contains("/")) 
                    {
                        Float[] valForMissing = new Float[]{};
                                                
                        if(Utils.isNumeric(dic.get(idxDic)[9])){
                            if(Pattern.matches(dic.get(idxDic)[0], "dilat")){
                                valForMissing = new Float[]{9f, 99f, 999f, 9999f, 99.99f, 99.9f, 99.09f, 88f, 20f};
                            } else if (Float.parseFloat(dic.get(idxDic)[9]) <= 8) {
                                valForMissing = new Float[]{9f, 99f, 999f, 9999f, 99.99f, 99.9f, 99.09f, 88f};
                            } else if (Float.parseFloat(dic.get(idxDic)[9]) <= 98) {
                                valForMissing = new Float[]{99f, 999f, 9999f, 99.99f, 99.9f, 99.09f, 88f};
                            } else if (Float.parseFloat(dic.get(idxDic)[9]) <= 998) {
                                valForMissing = new Float[]{999f, 9999f, 99.99f, 99.9f, 99.09f, 88f};
                            } else if (Float.parseFloat(dic.get(idxDic)[9]) <= 9998) {
                                valForMissing = new Float[]{ 9999f, 99.99f, 99.9f, 99.09f, 88f};
                            } 
                        }
                        
                        //Verifica os bounds
                        if (Utils.isNumeric(dataFramePrenacel.get(i)[idxColunas]) && Utils.isNumeric(dic.get(idxDic)[8]) && Utils.isNumeric(dic.get(idxDic)[9]) && 
                                (Float.parseFloat(dataFramePrenacel.get(i)[idxColunas]) < Float.parseFloat(dic.get(idxDic)[8])
                                || Float.parseFloat(dataFramePrenacel.get(i)[idxColunas]) > Float.parseFloat(dic.get(idxDic)[9])
                                && !Utils.isInArray(valForMissing, Float.parseFloat(dataFramePrenacel.get(i)[idxColunas])))) {
                            listOutOfBounds.add(new String[]{"Checar valores",
                                    dataFramePrenacel.get(i)[0],
                                    dataFramePrenacel.get(i)[6] + dataFramePrenacel.get(i)[7] ,
                                    dic.get(idxDic)[1],
                                    dic.get(idxDic)[0],
                                    dic.get(idxDic)[4],
                                    dataFramePrenacel.get(i)[idxColunas],
                                    dic.get(idxDic)[8],
                                    dic.get(idxDic)[9]});
                        }
                        else if((Utils.isNumeric(dataFramePrenacel.get(i)[idxColunas]) && Utils.isInArray(valForMissing, Float.parseFloat(dataFramePrenacel.get(i)[idxColunas]))))
                        {
                            dataFramePrenacel.get(i)[idxColunas] = "$$$";
                        }
                    }
//                    //Se a mulher não é elegivel, pula pra proxima
//                    if ((dataFramePrenacel.get(i)[idxColunas] != null && !dataFramePrenacel.get(i)[idxColunas].isEmpty()
//                            && !dataFramePrenacel.get(i)[idxColunas].equals("1")) ){
//                        
//                        idNaoElegivel.add(new String[]{"Não Elegivel",dataFramePrenacel.get(i)[0],
//                                         "(" + dataFramePrenacel.get(i)[1] + ";" + dataFramePrenacel.get(i)[2] + ";" + dataFramePrenacel.get(i)[3] + ";" 
//                                                 + dataFramePrenacel.get(i)[4] + ")"});
//                        
//                        if( (dataFramePrenacel.get(i)[idxDic-1] != null && !dataFramePrenacel.get(i)[idxDic-1].isEmpty() && dataFramePrenacel.get(i)[idxDic-1].equals("0"))) 
//                        {
//                            //System.out.println(dataFrameBold.get(i)[j]);
//                            idNaoElegivelMasMarcadoComoElegivel.add(new String[]{"Não Elegivel",
//                                dataFramePrenacel.get(i)[0],
//                                dataFramePrenacel.get(i)[6] + dataFramePrenacel.get(i)[7] ,
//                                dic.get(idxDic)[1],
//                                dic.get(idxDic)[0],
//                                dic.get(idxDic)[4],         
//                                dataFramePrenacel.get(i)[idxDic-12]});
//                            break;
//                        }
//                        break;
//                    }
                }
                
                if(colname.length > idxColunas + 1 && Pattern.matches(dic.get(idxDic)[0]+"___.*", 
                        colname[idxColunas]) && Pattern.matches(dic.get(idxDic)[0]+".*", colname[idxColunas+1])){
                    idxDic = idxDic - 1;
                }
                //System.out.println(j);
                idxColunas = idxColunas+1;
                idxDic = idxDic + 1;
            }
            
        }
                
        try {
            
            Listas lista = new Listas();
            listOutOfBounds = lista.removeCorrected(listOutOfBounds);
            listMissingValues = lista.removeCorrected(listMissingValues);
            listMissingValues = lista.removeCorrected(listMissingValues);
            
            //Utils.escreveCsv("ForaDosLimites.csv", listOutOfBounds, caminhoSalvar, true);
            Utils.escreveCsv("ValoresFaltantes.csv", listMissingValues, caminhoSalvar, true);
            Utils.escreveCsv("NãoElegivel.csv", idNaoElegivel, caminhoSalvar, true);
            
            if (!(new File(caminhoSalvar+"Hygia\\").exists())) 
            {
                 try{
                    new File(caminhoSalvar+"Hygia\\").mkdir();
                } 
                catch(SecurityException se){
                } 
            }
            
            Utils.escreveCsv("HygiaVazio.csv", listMissingHygia, caminhoSalvar+"Hygia\\", true);
            
            
            System.out.println("Total Undidades Prenacel: " + totalUbsPrenacel);   
            System.out.println("Total PRENACEL: " + totalPrenacel);            
            System.out.println("Total Parceiro: " + totalParceiro);
            System.out.println("Total Morbidade: " + totalMorbidade);
            System.out.println("Total Recusa: " + totalRecusa);            
            System.out.println("Total Não Digitado: " + totalNaoDigitado);

        } catch (IOException ex) {
            Logger.getLogger(Bounds.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dataFramePrenacel;
    }
}

