/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparadorprenacel;

import static comparadorprenacel.Utils.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author GLIDE-03
 */
public class ComparadorPrenacel {

    public static String[] cabecalho;
    
    public static int totalCasos;
    public static final String dataAnterior = "2016-06-03", dataAtual = "2016-06-06";
    public static final String caminhoRelatorio = "C:\\Users\\GLIDE-03\\Documents\\Prenacel\\Validacao dos Dados\\";
    public static final String pastaCSV = "CSV\\";
    
    public static final String caminhoBDNovo = caminhoRelatorio+dataAtual+"\\";
    public static final String caminhoBDAngito = caminhoRelatorio+dataAnterior+"\\";
    
    static List<String[]> bdPrenacel, bdPrenacelLimpo, dicionario; 
    public static List<String> idFormsAntigosCmProblemas;
    public static List<String> idFormsAtuaisCmProblemas;
        
    public static List<String> idFormsCorrigidos;
    
    public static List<String[]> lstAntigoVF, lstAtualVF,
                                lstAntigoFL, lstAtualFL, 
                                lstAntigoPL, lstAtualPL, 
                                lstNaoElegivel, lstAtualOutroTipo;
    
    public static int nCorrectedFields, nProblematicFields;
    
    public static void carregaBd()
    { 
        File folder = new File(caminhoBDNovo);
        for (File fileEntry : folder.listFiles()) {
            if(Pattern.matches("Prenacel_DATA_"+dataAtual+".*.csv", fileEntry.getName())){
                bdPrenacel = leCsv(caminhoBDNovo+fileEntry.getName());
            }
        }
        //dicionario = leCsvExcel(caminhoRelatorio+"Prenacel_DataDictionary_2015-09-29.csv");
        dicionario = leCsvExcel(caminhoRelatorio+"dic.csv");
        dicionario.remove(0);
    }
    
    public static int contarTodosProblemas(String caminho)
    {
        List<Integer> idProblemas = new ArrayList<>();        
        List<String[]> tipoListaAtual;
        List<String> variaveisCmProblema = new ArrayList<>();
        
        File folder = new File(caminho);
        for (File fileEntry : folder.listFiles()) {
            if(Pattern.matches(".*\\.csv", fileEntry.getName())){
                List<String[]> list = Utils.leCsv(fileEntry.getAbsolutePath());
                tipoListaAtual = new ArrayList<>();
                
                if(Pattern.matches("ProbLogicos.*", fileEntry.getName())){
                    Listas lista = new Listas();
                    list = lista.removeCorrected(list);
                }
                
                for (String[] prob : list) {
                    if(Utils.isInteger(prob[1]) && !idProblemas.contains(Integer.parseInt(prob[1]))){
                        idProblemas.add(Integer.parseInt(prob[1]));
                        variaveisCmProblema.add(prob[4]);
                        
                        if(prob.length > 2 && !idFormsAtuaisCmProblemas.contains(prob[2].replaceAll(" ", ""))){
                            idFormsAtuaisCmProblemas.add(prob[2].replaceAll(" ", ""));
                        }
                    }
                    nProblematicFields++;
                    tipoListaAtual.add(prob);
                }
                
//                if(Pattern.matches("ForaDosLimites.*", fileEntry.getName())){
//                    lstAtualFL = tipoListaAtual;
                //}else 
                if(Pattern.matches("ValoresFaltantes.*", fileEntry.getName())){
                    lstAtualVF = tipoListaAtual;
                }else if(Pattern.matches("NãoElegivel.*", fileEntry.getName())){
                    lstNaoElegivel = tipoListaAtual;
                }else if(Pattern.matches("ProbLogicos.*", fileEntry.getName())){
                    lstAtualPL = tipoListaAtual;
                }else{
                    lstAtualOutroTipo = tipoListaAtual;
                }
                
                nCorrectedFields = nCorrectedFields + RetornaNumCorrigidos(tipoListaAtual, fileEntry.getName());
            }
        }
                
//        System.out.println("\nCount all with frequency - Variaveis");
//        Map<String,Integer> frequency = new HashMap<String,Integer>();
//        for (String word:variaveisCmProblema)
//        {
//            Integer f = frequency.get(word);
//            //checking null
//            if(f==null) f=0;
//            frequency.put(word,f+1);
//        }
//        
//        for (Map.Entry<String, Integer> entrySet : frequency.entrySet()) 
//        {
//            String key = entrySet.getKey();
//            Integer value = entrySet.getValue();
//            System.out.println(key + ": " + value);
//        }
        
        criaArqProblemaPorCasoId();
        
        return idProblemas.size();
    }
        
    public static int RetornaNumCorrigidos(List<String[]> lstAtual, String arquivo)
    {
        List<String[]> lstAntigo = leCsv(caminhoBDAngito + pastaCSV + arquivo);
        
        int totalcorrigidos = 0;
        for (String[] antigo : lstAntigo) 
        {
            if(!idFormsAntigosCmProblemas.contains(antigo[2].replaceAll(" ", ""))) idFormsAntigosCmProblemas.add(antigo[2].replaceAll(" ", ""));
            boolean corrigido = true;
            for (String[] atual : lstAtual)
            {
                if(!idFormsAtuaisCmProblemas.contains(atual[2].replaceAll(" ", ""))) idFormsAtuaisCmProblemas.add(atual[2].replaceAll(" ", ""));
                int variaveisIguais=0;
                if(atual.length == antigo.length)
                    for (int i =0; i<atual.length; i++) 
                    {
                        //System.out.println(atual[i] + " " + antigo[i]);
                        if(atual[i].equals(antigo[i]))
                        {
                            variaveisIguais++;
                        }

                        if(variaveisIguais >= atual.length - 1)
                        {
                            corrigido = false;
                        }
                    }
            }
            if(corrigido){
                totalcorrigidos++;
                if(!idFormsCorrigidos.contains(antigo[2].replaceAll(" ", ""))) idFormsCorrigidos.add(antigo[2].replaceAll(" ", ""));
            }
        }
        return totalcorrigidos;
    }
    
     public static void imprimeReport(String caminhoNovo, String caminhoAngito)
    {
        
        List<String> cabAux = new ArrayList<>();
        //Seleciona o indice das colunas que tem o complete
        int j=0;
        while (j < cabecalho.length) {
            if (!cabecalho[j].contains("_complete")) {
                cabAux.add(cabecalho[j]);
            }
            j = j + 1;  
        }
        
        cabecalho = cabAux.toArray(new String[cabAux.size()]);
        
        nProblematicFields = 0;
        
        nCorrectedFields = 0;
        
        idFormsAntigosCmProblemas = new ArrayList<>();
        idFormsAtuaisCmProblemas = new ArrayList<>();
        
        idFormsCorrigidos = new ArrayList<>();
        
        contarTodosProblemas(caminhoBDNovo+pastaCSV);

        System.out.println("\n\n\nPrenacel\n");
        System.out.println("Erros Anteriores: " + idFormsAntigosCmProblemas.size());
        System.out.println("Total corrigido entre "+ dataAnterior + " e "+ dataAtual +": " + idFormsCorrigidos.size());
        System.out.println("\nNúmero de casos: " + totalCasos );
        System.out.println("Número de casos com incosistencias: "+ idFormsAtuaisCmProblemas.size() );
        System.out.println("% de casos com inconsistencia: " + ((float)idFormsAtuaisCmProblemas.size()/ totalCasos * 100) );
        System.out.println("Total de campos com inconsistencia: " + nProblematicFields );
        System.out.println("Média de campos com inconsistencia por caso: " + ((float)nProblematicFields/ idFormsAtuaisCmProblemas.size()));    
    }
    
    public static void criaArqProblemaPorCasoId()
    {
         List<String[]> allProblems = new ArrayList<String[]>();
         
         if(lstAtualVF != null)
            allProblems.addAll(lstAtualVF);
         if(lstAtualFL != null)
            allProblems.addAll(lstAtualFL); 
         if(lstAtualPL != null)        
            allProblems.addAll(lstAtualPL);
         if(lstAtualOutroTipo != null)
            allProblems.addAll(lstAtualOutroTipo);
         
         Utils lst = new Utils();
         
         allProblems = lst.OrdenaLista(allProblems, 2);
        
         String idReferencia = allProblems.get(0)[2];
         
         List<String[]> listaCaso = new ArrayList<>();
         for(String[] caso : allProblems){
             
             if(idReferencia.equals(caso[2])){
                 listaCaso.add(caso);
             }
             else
             {
                 try {
                     Utils.escreveCsv(idReferencia + ".csv", listaCaso, caminhoBDNovo+pastaCSV, true);
                 } catch (IOException ex) {
                     Logger.getLogger(ComparadorPrenacel.class.getName()).log(Level.SEVERE, null, ex);
                 }
                 listaCaso = new ArrayList<>();
                 listaCaso.add(caso);
                 idReferencia = caso[2];
             }
         }
         
     }
     
     
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //ComparaCsv();
        String caminhoNovo = caminhoBDNovo + "CSV\\";
        String caminhoAngito = caminhoBDAngito + "CSV\\";
        String caminhoLimpo = caminhoBDNovo + "Limpo\\";
        
        if (!(new File(caminhoNovo).exists())) 
        {
             try{
                new File(caminhoNovo).mkdir();
            } 
            catch(SecurityException se){
            } 
        }
                
        if (!(new File(caminhoLimpo).exists())) 
        {
             try{
                new File(caminhoLimpo).mkdir();
            } 
            catch(SecurityException se){
            } 
        }
        carregaBd();
        
        cabecalho = bdPrenacel.get(0);        
        bdPrenacel.remove(0); 
        
        totalCasos = bdPrenacel.size();
        
        bdPrenacel = Bounds.geraReport(cabecalho, bdPrenacel, dicionario, caminhoNovo);
        
        imprimeReport(caminhoNovo, caminhoAngito);
                
        contarTodosProblemas(caminhoNovo);
    }
    
}
