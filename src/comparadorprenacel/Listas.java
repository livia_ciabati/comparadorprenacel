/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparadorprenacel;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author GLIDE-03
 */
public class Listas {
    
    List<String[]> casosCorretos = new ArrayList<String[]>();
    
    List<String> questionariosRemoverTodosErros = new ArrayList<String>(){{ 
        add("A0108");     
        add("A0140");    
        add("A0161"); 
        add("A0167");
        add("B0005");  
    }};
    
    public Listas()
    {
        casosCorretos = Utils.leCsvExcel("C:\\Users\\GLIDE-03\\Documents\\Prenacel\\Validacao dos Dados\\all.csv");
    }
    
    public List<String[]> removeCorrected(List<String[]> problemas)
    {
        List<String[]> listFinal = new ArrayList<>(problemas);
        
        for(String[] probl : problemas)
        {
            int semelhante = 0 ;
            
            for(int i = 0; i< casosCorretos.size(); i++)
            {
                if( probl[1].replaceAll(" ", "").equals(casosCorretos.get(i)[1].replaceAll(" ", "")))
                {
                    int tamanho;
                    if(probl.length > casosCorretos.get(i).length)
                        tamanho = casosCorretos.get(i).length;
                    else
                        tamanho = probl.length;

                    for(int l=0; l<tamanho; l++)
                    {
                        if(probl[l].replace("\"", "").contains(casosCorretos.get(i)[l].replace("\"", "")))
                        {
                            semelhante++;
                        }
                    }
                    if(semelhante >= tamanho - 1)
                    {
                        //System.out.println(probl[1]);
                        listFinal.remove(probl);
                        break;
                    }
                }
            }
        }
        return listFinal;
    }

}
