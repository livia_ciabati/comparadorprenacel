/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparadorprenacel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author GLIDE-03
 */
public class Utils {
    
    //Delimiter used in CSV file
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    public Integer sortNum;
     
    public static Collection Subtract(Collection coll1, Collection coll2)
    {
        Collection result = new ArrayList(coll2);
        result.removeAll(coll1);
        return result;
    }
    
    public static boolean isInteger(String s) 
    {
        try { 
            Integer.parseInt(s); 
        } catch(NumberFormatException e) { 
            return false; 
        }
        // only got here if we didn't return false
        return true;
    }
    
    public static boolean isFloat(String s)
    {
        try { 
            Float.parseFloat(s); 
        } catch(NumberFormatException e) { 
            return false; 
        }
        // only got here if we didn't return false
        return true;
    }
    
    public static boolean isNumeric(String str)
    {
      return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
    
    public static void escreveCsv(String fileName, List<String[]> lista, String caminho, boolean formatExcel) throws IOException
    {
        if(caminho == null || caminho.isEmpty())
        {
            caminho = "";
        }
        
        if (!(new File(caminho).exists())) 
        {
            try{
                new File(caminho+"excel\\").mkdir();
            } 
            catch(SecurityException se){
            } 
        }
        
        if (!(new File(caminho+"excel\\").exists())) 
        {
            try{
                new File(caminho+"excel\\").mkdir();
            } 
            catch(SecurityException se){
            } 
        }
         
        FileWriter fileWriter = new FileWriter(caminho+fileName);

        //BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fileWriter,"UTF-8"));
                  
        if(formatExcel)
        { 
            escreveCsv(fileName, lista, caminho, false);
            fileWriter = new FileWriter(caminho+"excel\\"+fileName);
            fileWriter.append("sep=,\n");
        }
        
        for (String[] array : lista)
        {
            String prefix = "";
            for (String str : array)
            {
                fileWriter.append(prefix);
                prefix = COMMA_DELIMITER;
                fileWriter.append(str);
            }
            fileWriter.append(NEW_LINE_SEPARATOR);
        }

        //System.out.println("CSV file "+fileName+" was created successfully !!!");

        fileWriter.flush();
        fileWriter.close();
    }
   
    public static String getListAsCsvString(String[] list)
    {
         
        StringBuilder sb = new StringBuilder();
        for(String str:list){
            if(sb.length() != 0){
                sb.append(",");
            }
            sb.append(str);
        }
        String s = sb.substring(0,sb.length()-2)+"\n";
        return s;
    }
    
    public static List<String[]> leCsv(String arqNome)
    {
        List<String[]> linhas = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(arqNome)))
        {
            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                String[] resultado = sCurrentLine.split(",(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)",-1);
                if(resultado.length > 2)
                    linhas.add(resultado);
            }
           

        } catch (IOException e) {
            //e.printStackTrace();
            //System.out.println(e.getMessage());
            linhas = new ArrayList<>();
        } 
        return linhas;
    }
         
    public static List<String[]> leCsvExcel(String arqNome)
    {
        List<String[]> linhas = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(arqNome)))
        {
            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                String[] resultado = sCurrentLine.split(";(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)",-1);
                if(resultado.length > 2)
                    linhas.add(resultado);
            }
           

        } catch (IOException e) {
            e.printStackTrace();
            linhas = new ArrayList<>();
        } 
        return linhas;
    }
        
    public static boolean isInArray(Float[] array, float value)
    {
        for (Float flVal : array) {
            if(flVal == value) return true;
        }
        return false;
    }
        
    public static boolean isInArray(String[] array, String value)
    {
        for (String flVal : array) {
            if(flVal.equals(value)) return true;
        }
        return false;
    }

    public List<String[]> OrdenaLista(List<String[]> lista, int indice)
    {
        Collections.sort(lista, new UtilsComparador(indice));
        return lista;
    }
    

}

class UtilsComparador implements Comparator<String[]> {
    
    private int sortNum;
    public UtilsComparador(int indx){
        this.sortNum = indx;
    }
    
    @Override
    public int compare(String[] strings, String[] otherStrings) {
        return -1*(strings[sortNum].compareTo(otherStrings[sortNum]));
    }
}
